# cryptoex_network_estimator
A. Estimate Network Time via Public API `FetchOrderBook`

1. Run `npm install` for ccxt

2. Run `node tester.js`
-------------------
B. Estimate Network Time via Private API `FetchBalance`, `PlaceOrder`, `CancelOrder`, `FetchOpenOrders`

1. Run `npm install` for ccxt

2. Write protected.js with your own API key/secret/password  
    - `cp protected.js.sample protected.js`
    - Fill the strings with encoded below
    (new CipherUtil()).encrypt('ex_api_key_secret', 'any string you want to tell')

3. Set vars in tester.js
    - exchanges, symbol, price, qty, ...

4. Run `node tester.js private`

> Default exchanges are Binance, Kucoin, Poloniex, HuobiKorea, OkexKorea currently
