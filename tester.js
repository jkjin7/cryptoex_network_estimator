var ccxt = require('ccxt')
var CipherUtil = require('./CipherUtil.js')
var passphrase = 'dhekandpe_ekdll.,.!xx.'

async function getTokens(exchange_name) {
    const p = require('./protected.js')
    const tokens = {
        BINANCE: {api_key: p.BINANCE_KEY, api_secret: p.BINANCE_SEC},
        KUCOIN: {api_key: p.KUCOIN_KEY, api_secret: p.KUCOIN_SEC, api_pass: p.KUCOIN_EXT},
        POLONIEX: {api_key: p.POLONIEX_KEY, api_secret: p.POLONIEX_SEC},
        HUOBIKOREA: {api_key: p.HUOBIKOREA_KEY, api_secret: p.HUOBIKOREA_SEC},
        OKEXKOREA: {api_key: p.OKEXKOREA_KEY, api_secret: p.OKEXKOREA_SEC, api_pass: p.OKEXKOREA_EXT},
    }
    const api_key = (new CipherUtil()).decrypt(tokens[exchange_name].api_key, passphrase)
    const api_secret = (new CipherUtil()).decrypt(tokens[exchange_name].api_secret, passphrase)
    let api_pass
    if (tokens[exchange_name].api_pass)
        api_pass = (new CipherUtil()).decrypt(tokens[exchange_name].api_pass, passphrase)
    return [api_key, api_secret, api_pass]
}

async function init_ex(exchange_name, api_key, api_secret, api_pass) {
    let instance
    if (exchange_name == 'OKEXKOREA') {
        instance = new ccxt.okex3()
        const _endpoint = 'https://okex.co.kr'
        instance.hostname = _endpoint.split('//')[1]
        instance.urls.www = _endpoint
        instance.urls.api = _endpoint
    } else if (exchange_name == 'HUOBIKOREA') {
        instance = new ccxt.huobipro()
        const _endpoint = 'https://api-cloud.huobi.co.kr'
        instance.hostname = _endpoint.split('//')[1]
        instance.urls.api.market = _endpoint
        instance.urls.api.private = _endpoint
        instance.urls.api.public = _endpoint
    } else {
        instance = new ccxt[exchange_name.toLowerCase()]()
    }
    instance.name = exchange_name
    instance.apiKey = api_key
    instance.secret = api_secret
    instance.password = api_pass
    return instance
}

var exchanges = ['BINANCE', 'KUCOIN', 'POLONIEX', 'HUOBIKOREA', 'OKEXKOREA']
var symbol = 'XRP/USDT'
var side = 'buy'
var price = '0.2'  // !!! CAUTION WITH THIS !!!
var qty = '50'
var instances = {}
var i = instances

async function placeOrder(ex) {
    console.time(ex)
    const order = await instances[ex].createOrder(symbol, 'limit', side, qty, price)
    console.timeEnd(ex)
    return order
}
async function cancelOrder(ex, order_id) {
    console.time(ex)
    const res =  await instances[ex].cancelOrder(order_id, symbol)
    console.timeEnd(ex)
    let res_result = 
        res.status? res.status: 
        res.code? res.code:
        res.success? res.success:
        res.info? res.info.result: res.id
    if (['200000', 'canceled', 1].includes(res_result))
        res_result = true
    return res_result
}
async function fetchOpenOrders(ex) {
    console.time(ex)
    const res = await instances[ex].fetchOpenOrders(symbol)
    console.timeEnd(ex)
    return res
}

async function test1(ex) {
    var order = await placeOrder(ex)
    console.log(ex, 'Placed id:', order.id)
    let openOrders = await fetchOpenOrders(ex)
    console.log(ex, 'openOrder length, id:', openOrders.length, openOrders.length? openOrders[0].id: null)
    if (openOrders == []) {
        openOrders = await fetchOpenOrders(ex)
        console.log(ex, 'openOrder again!', openOrders.length, openOrders.length? openOrders[0].id: null)
    }
    if (openOrders == []) {
        openOrders = await fetchOpenOrders(ex)
        console.log(ex, 'openOrder again!!', openOrders.length, openOrders.length? openOrders[0].id: null)
    }
    console.log(ex, 'Cancel =>', await cancelOrder(ex, order.id))
    try {
        openOrders = await fetchOpenOrders(ex)
        console.log(ex, 'openOrder count (0 expected):', openOrders.length)
        if (openOrders.length) {
            openOrders = await fetchOpenOrders(ex)
            console.log(ex, 'openOrder count-2 (0 expected):', openOrders.length)
        }
        if (openOrders.length)
            console.log(ex, 'Cancel-2 =>', await cancelOrder(ex, order.id))
    } catch (err) {
        console.log(ex, 'openOrder count (return none):', 0)
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function main([private = '']) {
    private = private == 'private'? true: false
    
    // Init
    for (const ex of exchanges) {
        let api_key, api_secret, api_pass
        if (private) [api_key, api_secret, api_pass] = await getTokens(ex)
        instances[ex] = await init_ex(ex, api_key, api_secret, api_pass)
    }

    // Test FetchOrderBook
    const book_avg = {}
    const times_book = 11
    for (const i in [...Array(times_book)]) {
        for (const ins of Object.values(instances)) {
            const begin = Date.now()
            const book = await ins.fetchOrderBook(symbol, 5)
            console.log(ins.name, book.asks[0], book.bids[0] )
            const end = Date.now()
            const elapsed = end - begin
            console.log(ins.name+':', elapsed+'ms')
            if (i) {  // Skip first because it takes more time than 2nd+
                book_avg[ins.name] = Math.round(book_avg[ins.name]?
                    (book_avg[ins.name] * (Number(i) - 1) + elapsed) / Number(i):
                    elapsed
                )
            }
        }
    }
    console.log('---------------------------------')
    console.log(`Average Round Trip Time(ms) of FetchOrderBook * ${times_book}:`, book_avg)
    console.log('---------------------------------')

    if (!private) return
    
    // Test FetchBalance
    console.log('Balance test')
    for (const ins of Object.values(instances)) {
        console.time(ins.name)
        console.log(ins.name, (await ins.fetchBalance()).USDT )
        console.timeEnd(ins.name)
    }
    console.log('---------------------------------')

    // Test Place ~ FetchOpenOrders ~ Cancel ~ FetchOpenOrders
    for (const ex of exchanges) {
        try {
            await test1(ex)
            await sleep(2000)
        } catch (err) {
            console.error('err:', err)
        }
    }
}

const [,, ...processArgs] = process.argv
main(processArgs)
