'use strict';
const crypto = require('crypto');

// http://zzznara2.tistory.com/143
const Base64 = {

	// private property
	_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	// public method for encoding
	encode : function (input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		while (i < input.length) {

		  chr1 = input.charCodeAt(i++);
		  chr2 = input.charCodeAt(i++);
		  chr3 = input.charCodeAt(i++);

		  enc1 = chr1 >> 2;
		  enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		  enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		  enc4 = chr3 & 63;

		  if (isNaN(chr2)) {
			  enc3 = enc4 = 64;
		  } else if (isNaN(chr3)) {
			  enc4 = 64;
		  }

		  output = output +
			  this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
			  this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	// public method for decoding
	decode : function (input)
	{
	    var output = "";
	    var chr1, chr2, chr3;
	    var enc1, enc2, enc3, enc4;
	    var i = 0;

	    input = input.replace(/[^A-Za-z0-9+/=]/g, "");

	    while (i < input.length)
	    {
	        enc1 = this._keyStr.indexOf(input.charAt(i++));
	        enc2 = this._keyStr.indexOf(input.charAt(i++));
	        enc3 = this._keyStr.indexOf(input.charAt(i++));
	        enc4 = this._keyStr.indexOf(input.charAt(i++));

	        chr1 = (enc1 << 2) | (enc2 >> 4);
	        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	        chr3 = ((enc3 & 3) << 6) | enc4;

	        output = output + String.fromCharCode(chr1);

	        if (enc3 != 64) {
	            output = output + String.fromCharCode(chr2);
	        }
	        if (enc4 != 64) {
	            output = output + String.fromCharCode(chr3);
	        }
	    }

	    return output;
	}
}

class CipherUtil {
  unpadding(utf8str) {
    const numberOfPaddingBytes = utf8str.charCodeAt(utf8str.length - 1);
    //console.log("number of padding bytes : " + numberOfPaddingBytes);
    return utf8str.substring(0, utf8str.length-numberOfPaddingBytes);
  }

  // 암호화시 SALT 문자열, key 문자열은 utf8으로 인코딩 되어 있었기 때문에
  // NodeJS에서도 동일하게 처리
  generateSecretKey(valueAsInt) {
    const SALT = Buffer.from("sdkjhfeGHSEGjhwsdiujoh83904rudwklaa").toString('utf8');

    let key = Buffer.from((valueAsInt + (valueAsInt * valueAsInt)).toString()).toString('utf8');
    if (key.length > 16)
      key = key.substring(0, 16);
    else
      key = key + SALT.substring(0, 16-key.length).toString('utf8');

    //console.log('secretKey : ' + key);
    return key;
  }

  decrypt(encryptedData, key) {
    const generatedKey = this.generateSecretKey(key); // generateSecretKey2는 안됨. 꼭 generateSecretKey
    const decipher = crypto.createDecipheriv('aes-128-ecb', generatedKey, '');
    let decoded
    try {
        decoded = decipher.update(encryptedData, 'base64', 'utf8');
    } catch (err) {
        throw err
    }
    decoded += decipher.final('utf8');
    return decoded;
  }

  encrypt(rawData, key) {
    const generatedKey = this.generateSecretKey(key); // generateSecretKey2는 안됨. 꼭 generateSecretKey
    const cipher = crypto.createCipheriv('aes-128-ecb', generatedKey, '');
    cipher.setAutoPadding(true);
    let enc = cipher.update(rawData, 'utf8', 'base64');
    enc += cipher.final('base64');
    return enc;
  }
} // end of CipherUtil class

CipherUtil.Base64 = Base64;

module.exports = CipherUtil;
